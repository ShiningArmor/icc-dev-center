#!/usr/bin/env ruby


require 'watir'
require 'watir/extensions/element/screenshot'



def take_screenshot(url, browser, file_name)

  temp_dir = ENV["TEMP_DIR"]

  browser.goto url
  sleep 2
  browser.driver.save_screenshot("#{temp_dir}/#{file_name}.png")

end

browser = Watir::Browser.new :chrome

browser.window.resize_to(1920, 1080)

urls = { 'sonar-entity-model' => 'http://sonar.icc.local:9000/dashboard/index/45805',
         'sonar-mtb-core' => 'http://sonar.icc.local:9000/dashboard/index/45380',
		 'sonar-esb-db-integration' => 'http://sonar.icc.local:9000/dashboard/index/45933',
		 'sonar-test-module' => 'http://sonar.icc.local:9000/dashboard/index/45448',
		 'notification-preference-ws' => 'http://sonar.icc.local:9000/dashboard/index/45659',}

urls.each do |filename, url|

  take_screenshot(url, browser, filename)

end
