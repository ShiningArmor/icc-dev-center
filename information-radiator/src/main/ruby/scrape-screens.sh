#!/usr/bin/env bash

### Scripts are local to this directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export TEMP_DIR="screenshot"

TARGET_DIR=/c/git/monitor-js/new-software/TV10813

if [[ ! -e ${TEMP_DIR} ]]; then
    mkdir ${TEMP_DIR}
fi

ruby ${DIR}/scripts/tfs.rb

EXIT_CODE="$?"

if [[ "0" != "${EXIT_CODE}" ]]; then
    echo "Exiting with Error Code:${EXIT_CODE} Error Should Have been Provided" 
	exit "${EXIT_CODE}"
fi

ruby ${DIR}/scripts/sonar.rb

EXIT_CODE="$?"

if [[ "0" != "${EXIT_CODE}" ]]; then
    echo "Exiting with Error Code:${EXIT_CODE} Error Should Have been Provided" 
	exit "${EXIT_CODE}"
fi

ruby ${DIR}/scripts/jenkins.rb

EXIT_CODE="$?"

if [[ "0" != "${EXIT_CODE}" ]]; then
    echo "Exiting with Error Code:${EXIT_CODE} Error Should Have been Provided" 
	exit "${EXIT_CODE}"
fi


### Done Scraping, Now save the screens

/bin/cp -rf ${TEMP_DIR}/*.* ${TARGET_DIR}

pushd ${TARGET_DIR}

git pull

git add .

git commit -m "daily screenshot"

git push

popd

rm -rf ${TEMP_DIR}
