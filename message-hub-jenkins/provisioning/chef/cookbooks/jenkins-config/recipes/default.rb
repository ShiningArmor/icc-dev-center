#
# Cookbook Name:: build-server
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

### Some Logging
require 'logger'
require 'json'

STDOUT.sync = true
$logger = Logger.new(STDOUT)
$logger.level = Logger::DEBUG
$logger.progname=(File.basename(__FILE__))
$logger.debug  { "Start" }
###




# Install Jenkins Plug-ins

node['jenkins-config']['jenkins_plugins'].each_with_index do |plugin_with_version, index|

if plugin_with_version.nil?
	next
end

if ! plugin_with_version.include? ":"
	next
end

$logger.debug  { "Installing #{plugin_with_version}" }

  plugin, version = plugin_with_version.split(':')
  jenkins_plugin plugin do
    action :install
    version version
    install_deps false
  end
end

