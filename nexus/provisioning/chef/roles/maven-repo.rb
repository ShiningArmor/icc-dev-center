#Define a role that can include one or many Cookbooks / Recipies
#In the run_list the recipies names start with the directories below where cookbooks are
# https://docs.chef.io/roles.html

name "maven-repo"

    description "Call the local means to build a nexus server"
	
    run_list(
      'recipe[nexus-server]',
    )