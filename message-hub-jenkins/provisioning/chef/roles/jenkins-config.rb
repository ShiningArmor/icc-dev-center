#Define a role that can include one or many Cookbooks / Recipies
#In the run_list the recipies names start with the directories below where cookbooks are
# https://docs.chef.io/roles.html

name "jenkins-config"

    description "Install Plugins and whatnot for Jenkins"
	
    run_list(
      'recipe[jenkins-config]',
    )