#
# Cookbook Name:: build-server
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


###
# Install Jenkins and the Pre-Reqs
%w(java_se maven git::default jenkins::master).each do |recipe|
  include_recipe recipe
end
