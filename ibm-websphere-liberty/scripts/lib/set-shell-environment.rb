#!/usr/bin/env ruby

require 'logger'
require 'json'

STDOUT.sync = true
$logger = Logger.new(STDOUT)
$logger.level = Logger::ERROR
$logger.progname=(File.basename(__FILE__))

$logger.debug  { "Start" }

FILENAME = File.basename(__FILE__)

###
# Set some basic defaults that are primarily for the PoC
# These can be overridden by setting the ENV vars before calling this script
AWS_DEFAULT_REGION = "us-east-2"
AWS_DEFAULT_PROFILE = "saml"

AWS_DEFAULT_AMI = "ami-fcc19b99"

default_DEVOPS_BASE_URL = "devops.icct.com"

default_DEVOPS_CLIENT_ID = "devops"
default_DEVOPS_PROJECT_ID = "poc"
default_DEVOPS_PROJECT_ENV = "sbx"

env_vars = Hash.new

###
# Helper method for checking if an ENV variable is set for the current process
def env_isset(name)
  ENV.has_key?(name) && !ENV[name].nil? && !ENV[name].length.zero?
end


####
# These are the 4 key elements that allow the PoC to operate in the same account for changing customers, projects, and environments

if env_isset('DEVOPS_CLIENT_ID')
  default_DEVOPS_CLIENT_ID = ENV['DEVOPS_CLIENT_ID']
else
  env_vars['DEVOPS_CLIENT_ID'] = default_DEVOPS_CLIENT_ID
end

if env_isset('DEVOPS_PROJECT_ID')
  default_DEVOPS_PROJECT_ID = ENV['DEVOPS_PROJECT_ID']
else
  env_vars['DEVOPS_PROJECT_ID'] = default_DEVOPS_PROJECT_ID
end

if env_isset('DEVOPS_PROJECT_ENV')
  default_DEVOPS_PROJECT_ENV = ENV['DEVOPS_PROJECT_ENV']
else
  env_vars['DEVOPS_PROJECT_ENV'] = default_DEVOPS_PROJECT_ENV
end

# The FQDN of a base devops project.
# Typically not used in this form but will be pre-pended with additional values
# This will be used in other env variables to create an AWS bucket name
if env_isset('DEVOPS_BASE_URL')
  default_DEVOPS_BASE_URL = ENV['DEVOPS_BASE_URL']
else
  env_vars['DEVOPS_BASE_URL'] = default_DEVOPS_BASE_URL
end


####
# Following here all of these values are calculated from the 4 key elements
# And will override any existing ENV set


#  The project key.  A combination of the Client and Application IDs
project_key = default_DEVOPS_PROJECT_ID + "-" + default_DEVOPS_CLIENT_ID
env_vars['DEVOPS_PROJECT_KEY'] = project_key

# The Project Key with the ENV prepended.  This will be used to distinguish different environments within the same account
env_key = default_DEVOPS_PROJECT_ENV + "-" + project_key
env_vars['DEVOPS_ENV_KEY'] = env_key

if ! env_isset('AWS_DEFAULT_REGION')
  env_vars['AWS_DEFAULT_REGION'] = AWS_DEFAULT_REGION
end

if ! env_isset('AWS_DEFAULT_AMI')
  env_vars['AWS_DEFAULT_AMI'] = AWS_DEFAULT_AMI
end

if ! env_isset('AWS_DEFAULT_PROFILE')
  env_vars['AWS_DEFAULT_PROFILE'] = AWS_DEFAULT_PROFILE
end

if ! env_isset('AWS_PROFILE')
  env_vars['AWS_PROFILE'] = AWS_DEFAULT_PROFILE
end


branch_name = %x(git rev-parse --abbrev-ref HEAD).chomp

env_vars['DEVOPS_BRANCH_NAME'] = branch_name

scm_id = %x(git rev-parse #{branch_name}).chomp

env_vars['DEVOPS_SCM_ID'] = scm_id

env_vars['DEVOPS_BUILD_TIMESTAMP'] = Time.now.strftime("%Y-%m-%d %H:%M:%S")


$stdout.write env_vars.to_json

