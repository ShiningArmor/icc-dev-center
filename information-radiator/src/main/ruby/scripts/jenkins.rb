#!/usr/bin/env ruby

require 'watir'
require 'watir/extensions/element/screenshot'

temp_dir = ENV["TEMP_DIR"]
file_name = "jenkins-blue-ocean"

browser = Watir::Browser.new :chrome

browser.window.resize_to(1920, 1080)

begin

### Get Jenkins Dashboard
browser.goto "http://10.1.15.225:8080/job/MnT%20Message%20Hub/"

Watir::Wait.until { browser.title.include? "default" }

table = browser.table( :id, "projectstatus" )

if ! table.exists?
  raise "Table was not found"
end

header_row = table[0]

 header_row.cells.to_a.index do |cell|
 
	if cell.link.text.start_with?("Last Success")
		  cell.link.click
	end
end

browser.driver.save_screenshot("#{temp_dir}/#{file_name}.png")

rescue Exception => e 

browser.driver.save_screenshot("#{temp_dir}/#{e.to_s}.png")

raise e
end