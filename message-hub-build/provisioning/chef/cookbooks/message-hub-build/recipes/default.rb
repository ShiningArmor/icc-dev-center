#
# Cookbook Name:: build-server
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

###
# Configure Maven, Update the setting file etc, user to access the Git Repo
%w(configure-maven).each do |local_recipe|
  include_recipe "::#{local_recipe}"
end

###
# Install the SSH user to access the Git Repo
%w(install-default-user).each do |local_recipe|
  include_recipe "::#{local_recipe}"
end

###
# Install the Global Tool Configuration
# ???

###
# Install the Jobs
###
# Install the SSH user to access the Git Repo
%w(install-job-default).each do |local_recipe|
  include_recipe "::#{local_recipe}"
end