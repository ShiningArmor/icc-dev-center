#
# Cookbook Name:: build-server
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


###
# Install the Pre-Reqs and WebSphere Liberty Platform
%w(java_se wlp::default).each do |recipe|
  include_recipe recipe
end
