#!/usr/bin/env ruby

require 'zip'

# This is a simple example which uses rubyzip to
# recursively generate a zip file from the contents of
# a specified directory. The directory itself is not
# included in the archive, rather just its contents.
#
# Usage:
#   directory_to_zip = "/tmp/input"
#   output_file = "/tmp/out.zip"
#   zf = ZipFileGenerator.new(directory_to_zip, output_file)
#   zf.write()
class ZipFileGenerator
  # Initialize with the directory to zip and the location of the output archive.
  def initialize(input_dir, output_file)
    @input_dir = input_dir
    @output_file = output_file
  end

  # Zip the input directory.
  def write
    entries = Dir.glob(@input_dir) - %w(. ..)
	
	puts entries
	
    ::Zip::File.open(@output_file, ::Zip::File::CREATE) do |io|
      write_entries entries, '', io
    end
  end

  private

  # A helper method to make the recursion work.
  # Appears to require the full path
  def write_entries(entries, path, io)
  
    entries.each do |e|
      zip_file_path = path == '' ? e : File.join(path, e)
	   
      disk_file_path = zip_file_path ###File.join(@input_dir, zip_file_path)
	   
      ignore = [ ".git","target",".settings",".vagrant","mtb-messagehub-source.zip",".project",".classpath",".factorypath",".jsdtscope" ]
   
      if ignore.any? { |s| disk_file_path.include? s }
  	  
	    puts "Skipping:  " + disk_file_path

      else
	   
        puts "Zipping #{disk_file_path}"

        if File.directory? disk_file_path
          recursively_zip_directory(disk_file_path, io, zip_file_path)
        else
          put_into_archive(disk_file_path, io, zip_file_path)
        end
      end
	end
  end

  def recursively_zip_directory(disk_file_path, io, zip_file_path)
  
    io.mkdir zip_file_path
    subdir = Dir.entries(disk_file_path) - %w(. ..)
    write_entries subdir, zip_file_path, io
  end

  def put_into_archive(disk_file_path, io, zip_file_path)
  
    puts "Putting #{disk_file_path} into #{zip_file_path}"

  
    io.get_output_stream(zip_file_path) do |f|
      f.write(File.open(disk_file_path, 'rb').read)
    end
  end
end



directory_to_zip = "mtb*"
output_file = "mtb-messagehub-source.zip"
pwd = Dir.pwd

if File.exist?( output_file )
	File.delete( output_file )
end

puts pwd
### Use the main directory where we assume all of the projects will be availble to create source
Dir.chdir("../..") do

zf = ZipFileGenerator.new(directory_to_zip, pwd + "/" + output_file)
zf.write()

end
