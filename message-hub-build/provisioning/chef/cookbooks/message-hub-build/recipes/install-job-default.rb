#
# Cookbook Name:: build-server
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

job_name = "job-default"

  config = File.join(Chef::Config[:file_cache_path], "#{job_name}.xml.erb")
  cookbook_file config

  # Test basic job creation
  jenkins_job job_name do
    config config
    action :create
  end