#!/usr/bin/env ruby

# Requires certain ENV variables to be set or available
# Write all of the ENV Variables to a JSON file that will be available on the PHP server
# This file is called by BOTH Vagrant and Packer.  Put common ENV vars here

require 'pathname'
require 'fileutils'
require 'logger'
require 'json'

$stdout.sync = true

$logger = Logger.new(STDOUT)
$logger.level = Logger::ERROR
$logger.progname=(File.basename(__FILE__))

$logger.debug  { "Start" }

# Get us to a standard place.
$root_dir = File.expand_path('../..', File.dirname(__FILE__))
$root_dir.chomp!

$logger.debug  { "Root Dir: #{$root_dir}" }


#Add any additional variables to the ENV before it's written to a file
ENV['DEVOPS_PROJECT_KEY'] = ENV['DEVOPS_PROJECT_ID'] + "-" + ENV['DEVOPS_CLIENT_ID']

File.open("#{$root_dir}/current-environment.json","w") do |f|
  f.write(ENV.to_hash.to_json)
end

$logger.debug  { "End" }
