#Define a role that can include one or many Cookbooks / Recipies
#In the run_list the recipies names start with the directories below where cookbooks are
# https://docs.chef.io/roles.html

name "build-server"

    description "Call the local cookbook build-server"
	
    run_list(
      'recipe[build-server]',
    )