#!/usr/bin/env ruby

require 'net/http'


file_suffix = ".ear"
regex = "-\\d\\.\\d\\.\\d-\\d{8}\\.\\d{6}-\\d\.ear"

file_prefix = "notification-delivery-deployable"
path = "/nexus/content/groups/public/com/mtb/message-hub/notification-delivery-deployable/2.0.0-SNAPSHOT/"

# Must be somedomain.net instead of somedomain.net/, otherwise, it will throw exception.
Net::HTTP.start("nexus.icc.local") do |http|

    response = http.get(path)
	
	body = response.body
	
	regExpr = Regexp.new( file_prefix + regex )
	
	ear_file_name = body.scan(regExpr)

    length = ear_file_name.length

	full_path = path + ear_file_name[length-1]

    response = http.get(full_path)

    open(file_prefix + file_suffix, "wb") do |file|
        file.write(response.body)
    end
	
end


file_prefix = "notification-pref-deployable"
path = "/nexus/content/repositories/snapshots/com/mtb/message-hub/notification-pref-deployable/2.0.0-SNAPSHOT/"

# Must be somedomain.net instead of somedomain.net/, otherwise, it will throw exception.
Net::HTTP.start("nexus.icc.local") do |http|

    response = http.get(path)
	
	body = response.body
	
	regExpr = Regexp.new( file_prefix + regex )
	
	ear_file_name = body.scan(regExpr)
		
    length = ear_file_name.length

	full_path = path + ear_file_name[length-1]
	
    response = http.get(full_path)

    open(file_prefix + file_suffix, "wb") do |file|
        file.write(response.body)
    end
	
end

puts "Done."