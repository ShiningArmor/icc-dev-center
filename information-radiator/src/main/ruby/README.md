Screenshotting script written for the Sonar reporting application in Ruby and Bash

To use, clone the monitor-js repo @ http://gogs.icc.local/spreston/monitor-js and change the paths in sonar.sh accordingly

Also, copy and paste the chromedriver from the drivers folder into the $PATH of your machine

= Setup =

Run 'bundle install' 


== Finding the Chrome Driver ==

Download and put in your 'path'

https://sites.google.com/a/chromium.org/chromedriver/

= Getting Cmd Line for TFS =

https://github.com/Microsoft/team-explorer-everywhere/releases

Download the latest, TEE-CLC-14.111.1.zip

or install the plugin in eclipse

