#Define a role that can include one or many Cookbooks / Recipies
#In the run_list the recipies names start with the directories below where cookbooks are
# https://docs.chef.io/roles.html

name "message-hub-build"

    description "Add the Jobs for message hub building"
	
    run_list(
      'recipe[message-hub-build]',
    )