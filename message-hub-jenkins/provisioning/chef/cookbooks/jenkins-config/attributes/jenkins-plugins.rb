


#<> Disable Jenkins 2.0 setup wizard - currently until this is fixed: https://github.com/chef-cookbooks/jenkins/pull/471
default['jenkins']['master']['jvm_options'] = '-Djenkins.install.runSetupWizard=false'

#<> Where the private key for chef-client to communicate with Jenkins is stored on disk.
#default['build-server']['jenkins_private_key_path'] = '/etc/chef-jenkins-api.key'

#<> List of plugins to install
# Plugins:
# Retrieve this list via Jenkins Script console:
#
# Jenkins.instance.pluginManager.plugins.sort().each{ plugin -> println ("  ${plugin.getShortName()}:${plugin.getVersion()}") }
#
default['jenkins-config']['jenkins_plugins'] = %w(


# Structs, no deps
   structs:1.6

# SCM API, no deps
  scm-api:2.1.1

# Script Security, no deps
  script-security:1.27  

# JavaScript GUI Lib ACE Editor bundle, no deps
  ace-editor:1.1
  
# JavaScript GUI Lib jQuery bundles (jQuery and jQuery UI), no deps
  jquery-detached:1.2.1
  
# Variant, no deps
  variant:1.1
  
###  Support Core

# Credentials, no deps
  credentials:2.1.13
  
# Jackson 2 API, no deps
  jackson2-api:2.7.3
  
# Async Http Client, no deps
  async-http-client:1.7.24.1

# SCM API, no deps
  scm-api:2.1.1

# Metrics
  metrics:3.1.2.9  
  
# Support Core
  support-core:2.39
  
# Pipeline API
  workflow-api:2.12

# Pipeline Step API
  workflow-step-api:2.9
  
# Pipeline SCM Step
  workflow-scm-step:2.4

# Pipeline Supporting APIs
  workflow-support:2.13
  
# Pipeline Groovy
  workflow-cps:2.29 
  
# Pipeline Multibranch
  workflow-api:2.12

# Pipeline Job
  workflow-job:2.10

# Folders
  cloudbees-folder:6.0.3
  
# Branch API
  branch-api:2.0.8

# jUnit
  junit:1.20
  
# Deps for BlueOcean Display URL
  display-url-api:1.1.1
  
# Pipeline Multibranch
  workflow-multibranch:2.14

# SSH Credentials
  ssh-credentials:1.13
  
# Git client
  git-client:2.4.0

# Mailer
  mailer:1.20

# Matrix Project
  matrix-project:1.9

# Token Macro, no deps
  token-macro:2.0

# Javadoc Plugin, no deps
  javadoc:1.4
  
# Maven Integration
  maven-plugin:2.15.1

# Rebuilder, no deps
  rebuild:1.25

# Project Inheritance
  project-inheritance:2.0.0

#Run Condition
  run-condition:1.0
  
# Conditional BuildStep
  conditional-buildstep:1.3.1

# Conditional BuildStep
  conditional-buildstep:1.3.5

# Parameterized Trigger
  parameterized-trigger:2.33
  
# Job DSL
  job-dsl:1.58

# Promoted Builds
  promoted-builds:2.28.1

# Git
  git:3.1.0

# GitHub API, no deps
  github-api:1.85

# Plain Credentials
  plain-credentials:1.4

# GitHub
  github:1.26.1
  
# GitHub Branch Source
  github-branch-source:2.0.4

# Favorite
  favorite:2.0.4

# Durable Task, no deps
  durable-task:1.13
  
# Pipeline Nodes and Processes
  workflow-durable-task-step:2.10

# Pipeline Input Step
  pipeline-input-step:2.5

# Pipeline Stage Step
  pipeline-stage-step:2.2
  
# Pipeline Graph Analysis
  pipeline-graph-analysis:1.3

# Pipeline Shared Groovy Libraries
  workflow-cps-global-lib:2.7

# GIT server
  git-server:1.7
  
# Pipeline Basic Steps
  workflow-basic-steps:2.4
  
# Credentials Binding
  credentials-binding:1.10

# Icon Shim, no deps
  icon-shim:2.0.3

# Authentication Tokens API
  authentication-tokens:1.3
  
# Docker Commons
  docker-commons:1.6

# Docker Pipeline
  docker-workflow:1.10

# Pipeline Model API
  pipeline-model-api:1.1.1

# Pipeline Declarative Extension Points API
  pipeline-model-extensions:1.1.1

# Pipeline Stage Tags Metadata, no deps
  pipeline-stage-tags-metadata:1.1.1

# Pipeline  Model Definition
  pipeline-model-definition:1.1.1
  
# Pub-Sub "light" Bus, no deps
  pubsub-light:1.7

# Server Sent Events (SSE) Gateway
  sse-gateway:1.15
  
# Events API for Blue Ocean
  blueocean-events:1.0.0-rc2
  
  
# Blue Ocean Pre Reqs
  
  blueocean-autofavorite:0.6
  blueocean-commons:1.0.0-rc2
  blueocean-config:1.0.0-rc2
  blueocean-dashboard:1.0.0-rc2
  blueocean-display-url:1.5.1
  blueocean-git-pipeline:1.0.0-rc2
  blueocean-github-pipeline:1.0.0-rc2
  blueocean-i18n:1.0.0-rc2
  blueocean-jwt:11.0.0-rc2
  blueocean-personalization:1.0.0-rc2
  blueocean-pipeline-api-impl:1.0.0-rc2
  blueocean-pipeline-editor:0.2.0
  blueocean-rest:1.0.0-rc2
  blueocean-rest-impl:1.0.0-rc2
  blueocean-web:1.0.0-rc2


  blueocean:1.0.0-rc2



)