#!/usr/bin/env ruby

require "open3"

#
# Returns stdout on success, Runtime Error on failure or error
#
def syscall(*cmd)
  begin
    stdout, stderr, status = Open3.capture3(*cmd)
	
    if status.success?
      return stdout.slice!(0..-(1 + $/.size)) # strip trailing eol
	else
      raise "Error exec command: #{cmd};stdout=#{stdout};stderr=#{stderr};status=#{status}"
    end
  end
end


syscall( "vagrant destroy -f" )

syscall( "vagrant up" )

syscall( "vagrant package message-hub-jenkins" )

syscall( "vagrant box add package.box --name message-hub-jenkins --force" )

syscall( "rm -f *.box" )

