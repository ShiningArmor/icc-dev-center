#
# Cookbook Name:: message-hub-build
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

jenkins_user = "jenkins"
jenkins_user_home = "/var/lib/jenkins"
m2_home = "#{jenkins_user_home}/.m2"

directory '.m2' do
  path "#{jenkins_user_home}"
  owner "#{jenkins_user}"
  group "#{jenkins_user}"
  mode '0744'
  action :create
end

cookbook_file  'settings.xml' do
  source 'settings.xml'
  path "#{m2_home}"
  owner "#{jenkins_user}"
  group "#{jenkins_user}"
  mode '0444'
  action :create
end

