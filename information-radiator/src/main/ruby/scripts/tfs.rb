#!/usr/bin/env ruby


require 'watir'
require 'watir/extensions/element/screenshot'

temp_dir = ENV["TEMP_DIR"]
file_name = "burndown"

browser = Watir::Browser.new :chrome

browser.window.resize_to(1920, 1080)

begin
browser.goto "https://icc-2483.visualstudio.com/"

Watir::Wait.until { browser.title.include? "Sign in to your account" }

browser.text_field(:id => 'cred_userid_inputtext').set 'gbonk@icct.com'

browser.button(:id => 'cred_sign_in_button').click

Watir::Wait.until { ["Projects -","Work items -"].any? { |text| browser.title.include? text } }

rescue Exception => e 

browser.driver.save_screenshot("#{temp_dir}/#{e.to_s}.png")

raise
end

# Get the Backlog

browser.goto "https://icc-2483.visualstudio.com/MessageHubIntegration/_backlogs"

Watir::Wait.until { browser.title.include? "MessageHubIntegration Team" }

browser.driver.save_screenshot("#{temp_dir}/backlog.png")

# Get the Burn down

browser.div( :class, /burndown-chart/ ).image( :alt, /The burndown chart/).click

sleep 2

browser.driver.save_screenshot("#{temp_dir}/#{file_name}.png")

