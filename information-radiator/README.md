#Introduction 

This the script that ICC used in-house to update the "monitors" for the pods in the ICC 

#Getting Started
You will need to have Ruby installed. It comes by default with ChefDK

You also need to have the monitor-js project checked out as well

`git clone gogs@gogs.icc.local:spreston/monitor-js.git`

Run the Gemfile to make sure that all dependencies are available

`gem install bundler`

`bundle init`

#Build and Test

In Git Bash

From the root of the project, execute `src/main/ruby/scrape-screens.sh`

