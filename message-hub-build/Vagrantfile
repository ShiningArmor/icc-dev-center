# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'logger'
require 'fileutils'
require 'json'

$stdout.sync = true
logger = Logger.new(STDOUT)
logger.level = Logger::ERROR
logger.progname=(File.basename(__FILE__))

logger.debug  { "Start" }

# Run a script to setup good defaults if they are not set already
# Then write the ENV json file to the application location
# Only run it on vagrant Up
if ARGV[0] == 'up' then
  env_vars_json = `ruby scripts/lib/set-shell-environment.rb`
  
  new_env = JSON.parse(env_vars_json)
   
  new_env.each do |key, array|
    ENV[key] = array
  end
    
  system( 'ruby scripts/lib/store-environment.rb' )
end

# 1.8.4 has a fix for a Chef-Solo bug that causes convergence to fail.
# (q.v. https://github.com/chef/chef/issues/5017 for more information)
Vagrant.require_version ">= 1.8.4"
def require_plugin(name, version='>=0')
  unless Vagrant.has_plugin?(name, version)
    puts "Vagrant plugin '#{name}' is not installed."
    puts "Suggest running: 'vagrant plugin install #{name}'"
    exit! 1
  end
end

Vagrant.configure("2") do |config|
  require_plugin('vagrant-berkshelf')
  config.berkshelf.enabled = true

  config.vm.provider :virtualbox do |vb, override|
    vb.memory = 4096
	vb.linked_clone = true if Vagrant::VERSION =~ /^1.8/
    require_plugin('vagrant-vbguest')
    override.vm.box = "message-hub-jenkins"
    # Change the timesync from every 20min to every 10 seconds.
    # q.v. https://github.com/fideloper/Vaprobash/blob/master/Vagrantfile#L147
    vb.customize [
      "guestproperty", "set", :id,
      "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000
    ]
  end


  #### Because I'm using uBuntu
  config.ssh.username = "ubuntu"
  config.vm.define "message-hub-build"
  
  config.vm.network :forwarded_port, guest: 8080, host: 8080

  config.vm.provision :shell, :inline => "sudo rm -f /etc/localtime && sudo ln -s /usr/share/zoneinfo/America/New_York /etc/localtime", run: "always"

  config.vm.provision :chef_solo do |chef|

    chef.roles_path = './provisioning/chef/roles'
    chef.cookbooks_path = './provisioning/chef/cookbooks'

    chef.add_role 'message-hub-build'
  end
end
